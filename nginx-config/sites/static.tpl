
#
# nginx - static-template 
#
# /etc/nginx/sites/static.conf
#
#https://library.linode.com/web-servers/nginx/configuration/front-end-proxy-and-software-load-balancing#sph_more-information

# cache_bypass http://serverfault.com/a/334924/182074

    
    root /srv/www/htdocs; 
    gzip on;

    # locations, expires on
    location /static {
        autoindex off;
        expires 1M;
        add_header Pragma "public";
        add_header Cache-Control "public, max-age=315360000";

    }
