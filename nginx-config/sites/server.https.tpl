
#
# nginx - server-https-template (HTTPS)
#
# /etc/nginx/sites/server.https.conf
# you might want to adjust all BIG_LETTER_PLACEHOLDERS
#
#


server {

    # global

        # pre 1.9.5
        # listen 443 ssl spdy default_server;

        # post 1.9.4 -> httpv2 enabled
        listen 443 ssl http2 default_server;

    
        # SNI FTW!!!
        server_name www.example.com;
        # server_name _;
        
        # some settings needed for easy config-setup
        
        set $origin ORIGIN_IP_OR_HOSTNAME_OR_UPSTREAM; 
        
        

        #
        #include /etc/nginx/conf.d/perftest.conf;
        #include /etc/nginx/conf.d/header_cleanup.conf;



        # gzip an expires
        gzip on;
        expires off; # aint no static server

        # logs
        access_log /var/log/nginx/server.access.log flog;
        error_log  /var/log/nginx/server.error.log;

        # root for error/down-pages
        root /srv/www/htdocs;

        # if you want a status_page
        include /etc/nginx/conf.d/status.conf;

    # /global
    
    


    # ssl-config
    # http://www.mare-system.de/guide-to-nginx-ssl-spdy-hsts/

      # HSTS
      add_header  Strict-Transport-Security "max-age=31536000; includeSubDomains";


      ssl_certificate     /etc/nginx/ssl/server.crt-combined;
      ssl_certificate_key /etc/nginx/ssl/server.key;

      ssl_session_cache shared:SSL:10m;
      ssl_session_timeout 240m;
      
      ssl_protocols       TLSv1 TLSv1.1 TLSv1.2;    # works_everywhere_solution
      #ssl_protocols      TLSv1.1 TLSv1.2;                # modern browsers only  
      ssl_prefer_server_ciphers on;

      # suggestion from sslabs -> PFS
      #ssl_ciphers EECDH+ECDSA+AESGCM:EECDH+aRSA+AESGCM:EECDH+ECDSA+SHA384:EECDH+ECDSA+SHA256:EECDH+aRSA+SHA384:EECDH+aRSA+SHA256:EECDH+aRSA+RC4:EECDH:EDH+aRSA:RC4:!aNULL:!eNULL:!LOW:!3DES:!MD5:!EXP:!PSK:!SRP:!DSS;

      # modern (mozilla-suggestions)
      #ssl_ciphers 'ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!3DES:!MD5:!PSK';
  
      # intermediate (mozilla)
      ssl_ciphers 'ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:CAMELLIA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA';
  
      # legacy (mozilla)
      #ssl_ciphers 'ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:ECDHE-RSA-DES-CBC3-SHA:ECDHE-ECDSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:DES-CBC3-SHA:HIGH:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA';
    
    

    # /ssl-config


    # proxy-template
    include /etc/nginx/conf.d/proxy.conf;

    # static-server-template
    # include /etc/nginx/sites/static.conf;
    
    
    location / {
      
      proxy_pass $scheme://$origin; 
    
    }
    



}



