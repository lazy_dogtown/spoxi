
#
# nginx - static-template 
#
# /etc/nginx/sites/static_cache.conf
#
# incase you want to cache certain urls with static content

# cache_bypass http://serverfault.com/a/334924/182074
# 


    

    # locations, caching ON
    # in http {}
    # proxy_cache_path    /var/run/nginx/cache1    levels=1:2  keys_zone=cache1:50m inactive=10m max_size=50m;
    #   
    location /static {
        root /srv/www/htdocs; 
        gzip on;
        expires 1d;
        
        proxy_cache             cache1;
        proxy_cache_key         $host$request_uri$is_args$args;
        proxy_cache_valid       200 301 302 10m;
        proxy_cache_valid       404 1m;
        proxy_cache_valid       any 15m;
        proxy_cache_use_stale   error timeout invalid_header updating;
        proxy_pass http://appservers;
        

    }
