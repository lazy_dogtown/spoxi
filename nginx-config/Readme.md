
# NGINX-Default-Config
#
#

- template for nginx-lbs and frontends
- original nginx-configs are found in contrib/
- this template should work with vaniall nginx and no 3rd party modules
  enabled

- config-files
    - nginx.conf - global conf
    - conf.d/ - defaults and basic setups
      - upstream.conf - loadbalancer and upstream
      - proxy.conf - proxy defaults
      - status.conf - location /status
      - perftest.conf - location /perftest
      - cache_purge.conf  
      - compression.conf  
      - error_pages.conf  
      - logging.conf  
      - security_headers.conf  
      - headers_cleanup.conf
      - simple_waf.conf  
    - sites/ - virtualhost - confs
      - server.http.conf - http-virtualhost - template
      - server.https.conf - https-settings, virtualhost-template 
    


# usefull defaults

- https://github.com/h5bp/server-configs-nginx

# needed 3rd party modules

- naxsi
- lua-resty
- headers_more
- sticky-ng

