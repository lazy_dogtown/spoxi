from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

naxsi_score = ["$SQL", "$RFI", "$TRAVERSAL", "$EVADE", "$XSS", "$UWA", "$ATTACK"]
rulesets_seeds = {'WEB_SERVER', 'APP_SERVER', 'WEB_APPS', 'SCANNER', 'MALWARE'}
whitelists_seeds = {'WORDPRESS', }
naxsi_mz = ["BODY", "ARGS", "HEADERS", "FILE_EXT", "$HEADERS_VAR:Cookie", "$HEADERS_VAR:Content-Type",
            "$HEADERS_VAR:User-Agent", "$HEADERS_VAR:Accept-Encoding", "$HEADERS_VAR:Connection"]


def get_db_session(database):
    sqlite_url = 'sqlite:///%s' % database
    engine = db.create_engine(sqlite_url)

    # initialize the db if it hasn't yet been initialized
    #db.create_all(engine)

    Session = db.sessionmaker(bind=engine)
    session = Session()

    return session
