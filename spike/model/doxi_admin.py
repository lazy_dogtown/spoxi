from time import strftime, localtime

from spike.model import db

from nxapi import rules


class DoxiAdmin(db.Model):
    __bind_key__ = 'doxi_admin_db'


    id = db.Column(db.Integer, primary_key=True)
    sid = db.Column(db.Integer, nullable=False)
    ruleset = db.Column(db.String(1024), nullable=False)
    active = db.Column(db.Integer, nullable=False, server_default="1")
    change_src = db.Column(db.String, nullable=False, server_default="custom")
    timestamp = db.Column(db.Integer, nullable=False)


    def __init__(self, sid, ruleset, active=1, change_src="custom", timestamp=0):
        self.sid = sid
        self.ruleset = ruleset
        self.active = active
        self.change_src = change_src
        self.timestamp = timestamp
        self.warnings = []
        self.errors = []


class DoxiAdminRuleSets(db.Model):
    __bind_key__ = 'doxi_admin_db'

    id = db.Column(db.Integer, primary_key=True)
    ruleset = db.Column(db.String(1024), nullable=False)
    active = db.Column(db.Integer, nullable=False, server_default="1")
    timestamp = db.Column(db.Integer, nullable=False)


    def __init__(self, ruleset, active=0, timestamp=0):
        self.ruleset = ruleset
        self.active = active
        self.timestamp = timestamp
        self.warnings = []
        self.errors = []


class DoxiAdminSettings(db.Model):
    __bind_key__ = 'doxi_admin_db'

    id = db.Column(db.Integer, primary_key=True)
    setting_key = db.Column(db.String(1024), nullable=False)
    setting_val = db.Column(db.String(1024), nullable=False)
    timestamp = db.Column(db.Integer, nullable=False)


    def __init__(self, setting_key, setting_val, timestamp):
        self.setting_key = setting_key
        self.setting_val = setting_val
        self.timestamp = timestamp
        self.warnings = []
        self.errors = []

class DoxiExports(db.Model):
    __bind_key__ = 'doxi_admin_db'

    id = db.Column(db.Integer, primary_key=True)
    comments = db.Column(db.String(1024), nullable=False)
    timestamp = db.Column(db.Integer, nullable=False)


    def __init__(self, comments, timestamp):
        self.comments = comments
        self.timestamp = timestamp
        self.warnings = []
        self.errors = []

class DoxiReloads(db.Model):
    __bind_key__ = 'doxi_admin_db'

    id = db.Column(db.Integer, primary_key=True)
    reload_id = db.Column(db.String(1024), nullable=False)
    reload_status = db.Column(db.String(1024), nullable=False)
    comments = db.Column(db.String(1024), nullable=False)
    timestamp = db.Column(db.Integer, nullable=False)


    def __init__(self, reload_id, reload_status, comments, timestamp):
        self.reload_id = reload_id
        self.reload_status = reload_status
        self.comments = comments
        self.timestamp = timestamp
        self.warnings = []
        self.errors = []



class DoxiSites(db.Model):
    __bind_key__ = 'doxi_admin_db'


    id = db.Column(db.Integer, primary_key=True)
    site = db.Column(db.String(1024), nullable=False)
    site_id = db.Column(db.String(1024), nullable=False)
    active = db.Column(db.Integer, nullable=False, server_default="1")
    def __init__(self, site, site_id):
        self.site = site
        self.site_id = site_id


class DoxiSitesConfig(db.Model):
    __bind_key__ = 'doxi_admin_db'


    id = db.Column(db.Integer, primary_key=True)
    site_id = db.Column(db.String(1024), nullable=False)
    setting = db.Column(db.String(1024), nullable=False)
    value = db.Column(db.String(1024), nullable=False)
    timestamp = db.Column(db.Integer, nullable=False)
    def __init__(self, site_id, setting, value):
        self.site_id = site_id
        self.setting = setting
        self.value = value 
    

