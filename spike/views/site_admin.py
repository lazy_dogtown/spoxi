from __future__ import with_statement
from flask import Blueprint, redirect, Response, current_app, send_from_directory, request, url_for, flash, render_template
from werkzeug.contrib.atom import AtomFeed
from functools import wraps
from datetime import datetime
from hashlib import sha512
from shutil import copytree, copy, copyfile 

from simplejson import dumps as jdumps
from simplejson import loads as jloads
from simplejson import dump as jdump
from simplejson import load as jload


import os, time 

#~ from spike.model.naxsi_rules import NaxsiRules as nx_rules
#~ from spike.model._admin import DoxiAdmin as dx_admin
#~ from spike.model.site_admin import DoxiAdminRuleSets as dx_rulesets
#~ from spike.model.site_admin import DoxiAdminSettings as dx_settings
#~ from spike.model.doxi_rules import NaxsiRules as dx_rules

from spike.model import get_db_session 

from spike.views import time_stamp

site_admin = Blueprint('site_admin', __name__)

#
# helperfuncs
#


@site_admin.route("/")
def index():

  # check if first run
  doxi_temp = current_app.config["DOXI_TEMP"]
  if not os.path.isdir(doxi_temp):
    print "INIT site_admin"
    doxi_init()
  db_session = get_db_session("spike/doxi_rules.db")
  db_session_adm = get_db_session("spike/site_admin.db")
  all_rules = db_session.query(dx_rules).order_by(dx_rules.sid.desc()).all()
  rules_dict = {}
  for r in all_rules:
    rules_string = r.fullstr()
    rules_dict[r.sid] = { "msg": r.msg, "detect": r.detection, "ruleset": r.ruleset, "active": r.active, "new": 1, "mz": r.mz, "rmks": r.rmks, "fullstr": rules_string }
    deact = db_session_adm.query(dx_admin).filter(dx_admin.sid == r.sid).order_by(dx_admin.id.desc()).first()
    if deact:
      rules_dict[r.sid]["active"] = deact.active 
      rules_dict[r.sid]["new"] = 0 
      
    #~ if not _rules:
      #~ flash("No rules found, please create one", "success")
      #~ return redirect(url_for("rules.new"))
  return render_template("site_admin/index.html", rules=rules_dict)


