from __future__ import with_statement
from flask import Blueprint, redirect, Response, current_app, send_from_directory, request, url_for, flash, render_template
from werkzeug.contrib.atom import AtomFeed
from functools import wraps
from datetime import datetime
from hashlib import sha512
from shutil import copytree, copy, copyfile 

from simplejson import dumps as jdumps
from simplejson import loads as jloads
from simplejson import dump as jdump
from simplejson import load as jload


import os, time 

from spike.model.naxsi_rules import NaxsiRules as nx_rules
from spike.model.doxi_admin import DoxiAdmin as dx_admin
from spike.model.doxi_admin import DoxiAdminRuleSets as dx_rulesets
from spike.model.doxi_admin import DoxiAdminSettings as dx_settings
from spike.model.doxi_rules import NaxsiRules as dx_rules

from spike.model import get_db_session 

from spike.views import time_stamp

doxi_admin = Blueprint('doxi_admin', __name__)

#
# helperfuncs
#

# need to check if export-info is to be displayed
@doxi_admin.context_processor
def check_export_needed():
  doxi_temp = current_app.config["DOXI_TEMP"]
  needs_export_file = "%s/needs_export.yes" % doxi_temp
  export_display = {}
  export_display["needs_export"] = "no"
  export_display["reload_pending"] = "no"
  if os.path.isfile(needs_export_file):
    print "NEEDS EXPORT"
    export_display["needs_export"]  = "yes"
  reload_pending = "%s/reload.pending" % doxi_temp
  if os.path.isfile(reload_pending):
    print "RELOAD PENDING"
    export_display["reload_pending"]  = "yes"
  return dict(export_display=export_display)

# check if locked due to reload pending
def check_if_reload_pending(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
      doxi_temp = current_app.config["DOXI_TEMP"]
      reload_pending = "%s/reload.pending" % doxi_temp
      if os.path.isfile(reload_pending):
        print "RELOAD PENDING"
        return redirect(url_for("doxi_admin.reload_pending"))      
      return f(*args, **kwargs)
    return decorated_function


#
# </helperfuncs
#




@doxi_admin.route("/")
def index():

  # check if first run
  doxi_temp = current_app.config["DOXI_TEMP"]
  if not os.path.isdir(doxi_temp):
    print "INIT doxi_admin"
    doxi_init()
  db_session = get_db_session("spike/doxi_rules.db")
  db_session_adm = get_db_session("spike/doxi_admin.db")
  all_rules = db_session.query(dx_rules).order_by(dx_rules.sid.desc()).all()
  rules_dict = {}
  for r in all_rules:
    rules_string = r.fullstr()
    rules_dict[r.sid] = { "msg": r.msg, "detect": r.detection, "ruleset": r.ruleset, "active": r.active, "new": 1, "mz": r.mz, "rmks": r.rmks, "fullstr": rules_string }
    deact = db_session_adm.query(dx_admin).filter(dx_admin.sid == r.sid).order_by(dx_admin.id.desc()).first()
    if deact:
      rules_dict[r.sid]["active"] = deact.active 
      rules_dict[r.sid]["new"] = 0 
      
    #~ if not _rules:
      #~ flash("No rules found, please create one", "success")
      #~ return redirect(url_for("rules.new"))
  return render_template("doxi_admin/index.html", rules=rules_dict)



@doxi_admin.route("/reload_pending")
def reload_pending():
  
  return render_template("doxi_admin/reload_pending.html")

@doxi_admin.route("/reload_status/<path:reload_status_id>")
def reload_status(reload_status_id):

  doxi_temp = current_app.config["DOXI_TEMP"]
  local_rules = "%s/local-rules" % doxi_temp
  restarts = "%s/restarts" % doxi_temp  
  reload_pending =  "%s/reload.pending" % doxi_temp
  

  status_file = "%s/%s/status.json" % (restarts, reload_status_id)
  if not os.path.isfile(status_file):
    flash("cannot find status_id: %s" % reload_status_id, "danger")
    reload_msg = {}
  else:
    reload_msg = jloads("".join(open(status_file, "r").readlines()))
    
  return render_template("doxi_admin/reload_status.html", reload_msg = reload_msg )


@doxi_admin.route("/reload_execute")
def reload_execute():
  
  reload_data = update_rules()
  
  print reload_data
  
  reload_status_id = reload_data["reload_id"]
  
  return redirect(url_for("doxi_admin.reload_status", reload_status_id  = reload_status_id))     
  


@doxi_admin.route("/config_test")
def config_test():
  
  flash("config_test not yet available", "info")
  return redirect(url_for("doxi_admin.index"))     


@doxi_admin.route("/export")
@check_if_reload_pending
def export():

  ets = time_stamp()

  doxi_temp = current_app.config["DOXI_TEMP"]
  local_rules = "%s/local-rules" % doxi_temp
  needs_export = "%s/needs_export.yes" % doxi_temp
  db_session = get_db_session("spike/doxi_rules.db")
  db_session_adm = get_db_session("spike/doxi_admin.db")
  all_rules = db_session_adm.query(dx_admin.sid).distinct().order_by(dx_admin.sid.desc())
  rules_dict = {}
  rules_count = 0
  for r in all_rules:
    rules_count += 1
    rules_data = db_session.query(dx_rules).filter(dx_rules.sid == r.sid).order_by(dx_rules.id.desc()).first()
    rules_adm = db_session_adm.query(dx_admin).filter(dx_admin.sid == r.sid).order_by(dx_admin.id.desc()).first()
    rules_prefix = ""
    rules_string = rules_data.fullstr()
    if rules_adm.active == 0:
      rules_prefix = "# DEACT because $REASONS \n"
      rf = []
      for l in rules_string.split("\n"):
        l = l.strip()
        if l[0] != "#":
          rf.append("# %s" % l)
        else:
          rf.append(l)
      rules_string = "\n".join(rf)
    rules_fullstring = "%s%s" % (rules_prefix,  rules_string)
    if rules_data.ruleset in rules_dict:
      rules_dict[rules_data.ruleset].append(rules_fullstring)
    else:
      rules_dict[rules_data.ruleset] = [rules_fullstring]
      
  
  for ruleset in rules_dict:
    f = open("%s/%s" % (local_rules, ruleset), "w")
    for e in  rules_dict[ruleset]:
      f.write("%s \n\n\n" % e)
    f.close()
  
  id_desc = "%s.%s" % (current_app.config["SECRET_KEY"], time.time())
  
  print "ID_DESC: %s " % id_desc 
  reload_id = sha512(id_desc).hexdigest()[0:8]
  
  reload_pending = "%s/reload.pending" % doxi_temp
  f = open(reload_pending, "w")
  f.write("id: %s \nexport done: %s \nwaiting for reload\n" % (reload_id, ets))
  f.close()  
  
  export_log_entry =  "%s - export done, id: %s, %s rules, %s rulesets, reload pending" % (reload_id, ets, rules_count, len(rules_dict))

  export_log(export_log_entry)
  os.unlink(needs_export)
  flash("%s" % export_log_entry, "success")
  return redirect(url_for("doxi_admin.index"))

def export_log(in_put):
  doxi_temp = current_app.config["DOXI_TEMP"]
  export_log = "%s/export.log" % doxi_temp
  f = open(export_log, "a")
  f.write("%s \n" % in_put)
  f.close()
  
  



@doxi_admin.route("/manage_rules", methods=["POST"])
@check_if_reload_pending
def manage_rules():



  itemset = {}
  
  itemset["action"] = {}
  itemset["ruleset"] = {}
  # thank you you stupid js-developerZ
  # a value SHOULD be 1 || 0 and not 1 || NIL
  itemset["action_ids"] = []
  

  
  ts = int(time.time())

  
  for item  in request.form:
    itemz = request.form.getlist(item)
    #~ print itemz[0]
    item_target = item.split(".")[0]
    #~ if item_target not in itemset:
      #~ continue 
    action_id = int(item.split(".")[1])

    if action_id == 42000467:
      print "precheck"
      print action_id
      print itemz[0]

    if item_target == "action":
      if itemz[0].lower() == "on":
        itemset[item_target][action_id] = 1        
    else:
      itemset[item_target][action_id] = itemz[0].lower()
    itemset["action_ids"].append(action_id)
  
  for aid in itemset["action_ids"]:
    if not aid in itemset["action"]:
      itemset["action"][aid] = 0
    
        
  
  # select * from doxi_admin where sid =  '42000467'

    # check if first run
  db_session = get_db_session("spike/doxi_rules.db")
  db_session_adm = get_db_session("spike/doxi_admin.db")
  os.system("ls -la spike/doxi_rules.db")
  action_items = itemset["action"] 
  ruleset_dict = itemset["ruleset"] 
  needs_export = 0
  for r in action_items:
    if r == 42000467:
      print r
      print action_items[r]
      print ruleset_dict[r]
    change_src = "default"
    execute = "yes"
    is_known = db_session_adm.query(dx_admin).filter(dx_admin.sid == r).order_by(dx_admin.id.desc()).first()
    if is_known:
      if int(is_known.active) == int(action_items[r]):
        execute = "no"
        
      else:
        change_src = "custom"
    if execute == "yes":
      needs_export = 1
      dx = dx_admin(sid=r, ruleset = ruleset_dict[r], active = action_items[r], change_src =  change_src, timestamp = ts)
      db_session_adm.add(dx)
      #~ print execute
  db_session_adm.commit()
  flash("rulesets updated", "success")
  if needs_export == 1:
    flash("export needed", "danger")
    export_pending_msg()
  return redirect(url_for("doxi_admin.index"))


@doxi_admin.route("/rulesets")
@check_if_reload_pending
def rulesets():

  db_session = get_db_session("spike/doxi_rules.db")
  db_session_adm = get_db_session("spike/doxi_admin.db")
  ts = int(time.time())


  rulesdb_rulesets =   db_session.query(dx_rules.ruleset).distinct()
  
  doxi_rulesets =  db_session_adm.query(dx_rulesets.ruleset).distinct()
  
  for r in rulesdb_rulesets:
    r_known = db_session_adm.query(dx_rulesets).filter(dx_rulesets.ruleset == r.ruleset).first()
    if not r_known:
      new_r = dx_rulesets(ruleset = r.ruleset, active=1, timestamp= ts)
      db_session_adm.add(new_r)
      db_session_adm.commit()
      flash("new ruleset added to doxi_admin <strong>%s</strong> "  % r.ruleset, "success")


  doxi_rulesets =  db_session_adm.query(dx_rulesets).distinct()
  
  ruleset_dict = {}
  for r in doxi_rulesets:
    r_vals = db_session_adm.query(dx_rulesets).filter(dx_rulesets.ruleset == r.ruleset).order_by(dx_rulesets.id.desc()).first()
    ruleset_dict[r.ruleset] = r_vals 
  

  return render_template("doxi_admin/rulesets.html", rulesets=ruleset_dict)

@doxi_admin.route("/manage_rulesets", methods=["POST"])
@check_if_reload_pending
def manage_rulesets():



  itemset = {}
  
  itemset["action"] = {}
  itemset["ruleset"] = {}
  # thank you you stupid js-developerZ
  # a value SHOULD be 1 || 0 and not 1 || NIL
  itemset["action_ids"] = []
  

  
  ts = int(time.time())

  
  for item  in request.form:
    itemz = request.form.getlist(item)[0]
    print item 
    print itemz
    #~ continue 
    #~ print itemz[0]
    item_target = item.split(".")[0]
    #~ if item_target not in itemset:
      #~ continue 
    action_id = ".".join(item.split(".")[1:])

    if action_id == 42000466:
      print "precheck"
      print action_id
      print itemz[0]

    if item_target == "action":
      if itemz.lower() == "on":
        itemset[item_target][action_id] = 1        
    else:
      itemset[item_target][action_id] = itemz
    itemset["action_ids"].append(action_id)
  
  
  for aid in itemset["action_ids"]:
    if not aid in itemset["action"]:
      itemset["action"][aid] = 0
    
  print itemset
  
  #~ return redirect(url_for("doxi_admin.rulesets"))
        
  
  # select * from doxi_admin where sid =  '42000466'

    # check if first run
  db_session = get_db_session("spike/doxi_rules.db")
  db_session_adm = get_db_session("spike/doxi_admin.db")
  action_items = itemset["action"] 
  ruleset_dict = itemset["ruleset"] 
  for r in action_items:
    print "........ " +  r
    execute = "no"
    is_same = db_session_adm.query(dx_rulesets).filter(dx_rulesets.ruleset == r).order_by(dx_rulesets.id.desc()).first()
    if is_same.active != int(action_items[r]) :
      execute = "yes"
    if execute == "yes":
      # here we adjust all the rules from doxi_rules an on/offline them 
      dx_change_rules = db_session_adm.query(dx_admin.sid).filter(dx_admin.change_src != "custom", dx_admin.ruleset == r).distinct()
      changed_count = dx_change_rules.count
      for cr in dx_change_rules:
        cr_new = dx_admin(sid=cr.sid, ruleset = r, active = action_items[r], change_src =  "ruleset", timestamp = ts)
        db_session_adm.add(cr_new)
      #~ print execute
      flash("%s rules updated from changed ruleset: %s" % (changed_count, r ), "success")
      new_r = dx_rulesets(ruleset = r, active=action_items[r], timestamp= ts)
      flash("ruleset updated: %s"  % (r ), "success")
      db_session_adm.add(new_r)

  db_session_adm.commit()
  flash("rulesets updated", "success")
  return redirect(url_for("doxi_admin.rulesets"))


@doxi_admin.route("/settings")
@check_if_reload_pending
def settings():
  
  rulesdb_rulesets =   db_session.query(nx_rules).distinct(nx_rules.ruleset).all()
  

  return render_template("doxi_admin/rulesets.html", rules=rules_dict)


@doxi_admin.route("/search", methods=["GET"])
@check_if_reload_pending
def search():
    terms = request.args.get('s', '')

    if len(terms) < 2:
        return redirect(url_for("rules.index"))

    # No fancy injections
    whitelist = set(string.ascii_letters + string.digits + ':-_ ')
    filtered = ''.join(filter(whitelist.__contains__, terms))

    if filtered.isdigit():  # get rule by id
        _rules = db.session.query(dx_rules).filter(NaxsiRules.sid == int(filtered))
    else:
        cve = re.search('cve:\d{4}-\d{4,}', filtered, re.IGNORECASE)  # search by CVE

        expression = '%' + filtered + '%'
        _rules = db.session.query(NaxsiRules).filter(
            db.or_(
                NaxsiRules.msg.like(expression),
                NaxsiRules.rmks.like(expression),
                NaxsiRules.detection.like(expression)
            )
        )
        if cve:
            _rules.filter(NaxsiRules.msg.like('%' + cve.group() + '%'))
    _rules = _rules.order_by(NaxsiRules.sid.desc()).all()
    return render_template("rules/index.html", rules=_rules, selection="Search: %s" % filtered, lsearch=terms)



@doxi_admin.route("/updates", methods=["POST"])
@check_if_reload_pending
def updates_admin():



  itemset = {}
  
  itemset["action"] = {}
  itemset["ruleset"] = {}
  # thank you you stupid js-developerZ
  # a value SHOULD be 1 || 0 and not 1 || NIL
  itemset["action_ids"] = []
  

  
  ts = int(time.time())

  
  for item  in request.form:
    itemz = request.form.getlist(item)
    #~ print itemz[0]
    item_target = item.split(".")[0]
    #~ if item_target not in itemset:
      #~ continue 
    action_id = int(item.split(".")[1])

    if action_id == 42000466:
      print "precheck"
      print action_id
      print itemz[0]

    if item_target == "action":
      if itemz[0].lower() == "on":
        itemset[item_target][action_id] = 1        
    else:
      itemset[item_target][action_id] = itemz[0].lower()
    itemset["action_ids"].append(action_id)
  
  for aid in itemset["action_ids"]:
    if not aid in itemset["action"]:
      itemset["action"][aid] = 0
  
        
  
  # select * from doxi_admin where sid =  '42000466'

    # check if first run
  db_session = get_db_session("spike/doxi_rules.db")
  db_session_adm = get_db_session("spike/doxi_admin.db")
  os.system("ls -la spike/doxi_rules.db")
  action_items = itemset["action"] 
  ruleset_dict = itemset["ruleset"] 
  for r in action_items:
    if r == 42000466:
      print r
      print action_items[r]
      print ruleset_dict[r]
    
    execute = "yes"
    is_known = db_session_adm.query(DoxiAdmin).filter(DoxiAdmin.sid == r).first()
    if is_known:
      if int(is_known.active) == int(action_items[r]):
        execute = "no"
    if execute == "yes":
      dx = DoxiAdmin(sid=r, ruleset = ruleset_dict[r], active = action_items[r], timestamp = ts)
      db_session_adm.add(dx)
      #~ print execute
  db_session_adm.commit()
  flash("rulesets updated", "success")
  return redirect(url_for("doxi_admin.index"))



def doxi_init():
  print """

> INIT Doxi_Admin_Mode

  """
  doxi_temp = current_app.config["DOXI_TEMP"]
  doxi_rules_repo = current_app.config["DOXI_RULES_REPO"]
  doxi_rules_src = "%s/doxi-rules/database/rules.db" % doxi_temp
  doxi_rules_dest = "spike/doxi_rules.db"
  local_rules = "%s/local-rules" % doxi_temp
  restarts = "%s/restarts" % doxi_temp
  os.mkdir(doxi_temp)
  os.mkdir(local_rules)
  os.mkdir(restarts)
  
  with cd(doxi_temp):
    os.system("git clone %s" % doxi_rules_repo)
  copyfile(doxi_rules_src, doxi_rules_dest)
  flash("Doxi_Admin INIT // rules.db copied", "success")
  print """

> INIT Done
  
  """

def doxi_update():
  print """

> UDPATE Doxi_Admin_Mode

  """

def export_pending_msg():

  doxi_temp = current_app.config["DOXI_TEMP"]
  needs_export_file = "%s/needs_export.yes" % doxi_temp
  f = open(needs_export_file, "w")
  ets = time_stamp()
  f.write("%s\n" % ets)
  f.close()

def call_process(command):
  from subprocess import Popen, PIPE
      
  print "> running: %s" % command
  p = Popen(command, stdout=PIPE, stderr=PIPE)
  output = p.communicate()
  return p.returncode == 0, '\n'.join(output).strip(' \n')

  
  

def update_rules():
  
  
  print "update and reload"

  doxi_temp = current_app.config["DOXI_TEMP"]
  local_rules = "%s/local-rules" % doxi_temp
  restarts = "%s/restarts" % doxi_temp  
  reload_pending =  "%s/reload.pending" % doxi_temp

  if not os.path.isfile(reload_pending):
    print "[i] no reload.pending, done"
    return(0)
  
  nginx_bin =  current_app.config["NGINX_BIN"]
  nginx_conf_base =  current_app.config["NGINX_CONF_BASE"]
  nginx_conf_file =  "%s/nginx.conf" % nginx_conf_base
  nginx_reload = current_app.config["NGINX_RELOAD"]
  doxi_live = "%s/doxi-rules" % nginx_conf_base
  
  reload_pending_txt = open(reload_pending).readlines()
  reload_id = None 
  for l in reload_pending_txt:
    l_kv = l.split(":")
    if l_kv[0] == "id":
      reload_id = l_kv[1].strip()
  
  if not reload_id:
    print "[-] cannot extract reload_id from %s" % reload_pending
    return(1)
  
  print reload_id 
  
  # creating filestructure
  reload_job_path = "%s/%s" % (restarts, reload_id)
  if not os.path.isdir(reload_job_path):
    os.makedirs(reload_job_path)
    for create_dir in ["rollback", "misc"]:
      mp = "%s/%s" % (reload_job_path, create_dir  )
      os.makedirs(mp)
  
  
  # creating files for rollback
  rollback = "%s/rollback" % reload_job_path
  try:  
    copytree("%s" % doxi_live, rollback )  
    print "[i] created rollback-dir"
  except:
    # happens if already exists
    print "[-] cannot copy files to rollback, maybe existing"
    
  
  # copying files to doxi-live
  src_files = os.listdir(local_rules)
  for file_name in src_files:
    full_file_name = os.path.join(local_rules, file_name)
    if (os.path.isfile(full_file_name)):
      print "[i] copying %s " % full_file_name
      copy(full_file_name, doxi_live)
  print "[i] copied new rulesets to %s " % doxi_live
  
  # checking nginx
  
  check_nginx_cmd = ("sudo %s -t -c %s" % (nginx_bin, nginx_conf_file)).strip()
  
  check_status, check_output = call_process(check_nginx_cmd.split(" "))
  
  output_dict = {}
  
  output_dict["reload_id"] = reload_id
  output_dict["exec_time"] = int(time.time())
  output_dict["check_status"] = check_status
  output_dict["check_output"] = check_output

  print check_output
  
  reload_status = "UNKNOWN"
  
  if check_status == True:
    # reload, we'll have a reload!
    
    flash("OK config_test : %s" % reload_id, "success")

    reload_nginx_cmd = ("%s" % (nginx_reload)).strip()
  
    reload_status, reload_output = call_process(reload_nginx_cmd.split(" "))
    
    output_dict["reload_status"] = reload_status
    output_dict["reload_output"] = reload_output    
    
    if reload_status == True:
      flash("OK nginx_reload : %s" % reload_id, "success")
      print "[+] reload successfull"
      reload_status = "Success"
      os.remove(reload_pending)
    else:
      flash("ERROR nginx_reload: %s" % reload_id, "danger")
      print "[-] ERROR: reload error"
      reload_status = "ERROR - Nginx.Reload.Error"
      
    
  
  else:
    flash("ERROR config_test: %s" % reload_id, "danger")
    print "[-] error while trying tro reload new rulesets: Check-Status: %s" % reload_id
    output_dict["reload_status"] = False
    output_dict["reload_output"] = "ERR: CONFIG_CHECK_ERROR; no reload initiated for %s" % reload_id    
    reload_status = "ERROR - Nginx.Configtest.Error"

  log_entry =  "%s - reload done, status: %s" % (reload_id, reload_status)
  export_log(log_entry)

  status_file = "%s/status.json" % reload_job_path
  f = open(status_file, "w")
  f.write(jdumps(output_dict))
  f.close()
  return(output_dict)
  
    
  # TODO rollback!
    
    
  
  
  

class cd:
    """Context manager for changing the current working directory"""
    def __init__(self, newPath):
        self.newPath = os.path.expanduser(newPath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)

