import time

def time_stamp():
  return(time.strftime("%F %H:%M", time.localtime(time.time())))
