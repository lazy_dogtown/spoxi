import base64
import os
import logging

from shutil import copytree, copy

from flask import Flask, current_app
from flask_bootstrap import Bootstrap

from spike.views import default, rules, rulesets, sandbox, whitelists, whitelistsets, doxi_admin, site_admin
from spike.model import db

from simplejson import dumps as jdumps
from simplejson import loads as jloads
from simplejson import dump as jdump
from simplejson import load as jload

version = "0.6.8 alpha (doxi-admin test)"


def create_app(config_filename=''):
    logging.info("Spike app.init()")

    app = Flask(__name__)

    if config_filename:
        app.config.from_pyfile(config_filename)

    if not app.config["SECRET_KEY"]:
        app.config["SECRET_KEY"] = base64.b64encode(os.urandom(128))

    # TODO: app.config["DOXI_TEMP"] into config
    if not "DOXI_RULES_REPO" in app.config:
        app.config["DOXI_RULES_REPO"] = "https://bitbucket.org/lazy_dogtown/doxi-rules"

    if not "DOXI_TEMP" in app.config:
        app.config["DOXI_TEMP"] = "doxi_temp"
    
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False


    #app.config["SQLALCHEMY_BINDS"] = {'rules': 'sqlite:///rules.db'}
    app.config["SQLALCHEMY_BINDS"] = {
    # db when creating rules
    'rules':        'sqlite:///rules.db',
    # settings, maybe TODO
    'settings':     'sqlite:///settings.db',
    # see Readme.doxi_admin.md
    'doxi_admin_db':   'sqlite:///doxi_admin.db',
    'doxi_rules_db':   'sqlite:///doxi_rules.db',

    }


    db.init_app(app)
    db.app = app

    Bootstrap(app)  # add bootstrap templates and css

    # add blueprints
    app.register_blueprint(default.default)
    app.register_blueprint(rules.rules, url_prefix='/rules')
    app.register_blueprint(rulesets.rulesets, url_prefix='/rulesets')
    app.register_blueprint(sandbox.sandbox, url_prefix='/sandbox')
    app.register_blueprint(whitelists.whitelists, url_prefix='/whitelists')
    app.register_blueprint(whitelistsets.whitelistsets, url_prefix='/whitelistsets')
    app.register_blueprint(doxi_admin.doxi_admin, url_prefix='/doxi_admin')
    app.register_blueprint(site_admin.site_admin, url_prefix='/site_admin')

    # register filters
    app.jinja_env.globals['version'] = version

    return app


  
  
    
  
  
  

  
  
  
  
  

  
  
  
