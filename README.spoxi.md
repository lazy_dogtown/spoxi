
# Spoxi - Spike! - Doxi-Admin-Mode


Spoxi is a tool to manage, update, activate/deactivate doxi-rules on an
naxsi/nginx - server

it consist of an interface and a 


## databases

- doxi_admin.db -> stores all settings on deactivated rules/rulesets and to manage sites
- doxi_rules.db -> current colne of original spike rules_db, updated via git_repo


## Ruleset

- when disabling rulesets, ALL rules that has not been managed and ship with "custom" flag will get deactivated




## Workflow

- edit rules
- export rules
- cronjob checks if new rules
- cronjob reloads new rules
- send mail upon success/error


## Setup

- setup spike && spike init
- adjust sudoers for spike-user/usergroup

~~~
# sudoers
# defining user_groups
User_Alias NGXADMINS       = spoxi

# defining commands
Cmnd_Alias RELOAD_JOBS = /etc/init.d/nginx, \
                /usr/sbin/nginx, \
                /usr/local/sbin/nginx, \
                /usr/local/nginx/sbin/nginx, \
                /usr/sbin/service

NGXADMINS ALL = (root) NOPASSWD:SETENV: RELOAD_JOBS

~~~

- install cronjob 
~~~

$ (crontab -l ; echo "*/2 * * * cd $HOME/spike && ./doxi-admin update_and_reload") | crontab -

~~~

## technical workflow

- original ruleset is located in doxi_temp/doxi-rules
- a set with information which rules/rules are to be activated is in
  spike/doxi_admin.db (sqlite-db)
- the master-rules-dir from which /etc/nginx/doxi-rules originates from 
  is in doxi_temp/local-rules

- edit rules
  - only changes in database
  - if not exists, file doxi_temp/needds_export.yes is written, 
  - only indicator-file for interface-display (export needed)
  
- export rules
  - an ID is created to be used as reference
  - file doxi_temp/reload.pending is written with ID and timestamp
  - as long as this file exists, no other export is possible
  
- a cronjob checks and runs "update_and_reload"
  - if not doxi_temp/reload.pending exists, GOTO 10 and sleep again
  - reload.pending and ID is read
  - an directory is created in restarts/ID and some sub-dirs for rollback and status.json
  - local rules with activation/deactivation-info is written into
    doxi_temp/local-rules 
  - current /etc/nginx is written to restarts/ID/rollback
  - local rules are copied to /etc/nginx/doxi-rules
  - nginx is tested, if successfull, reload, on error rollback
  - file doxi_temp/reload.pending is deleted
  - logentries are created

- send mail upon success/error

## resetting everything

- delete doxi_temp and all old db-files 
- re-run init 

~~~

rm -Rf doxi_temp/
rm spike/*.db

./doxi-admin init 

~~~


## Dockerinstall (for testings or deployments

- src: docker-install

- workflow:
  - docker-build.sh -> Dockerfile.spoxi
    - inside container root:
      - build.sh
      - preinst.sh
    - inside containter / spoxi ~/spoxi-ctl
      - install.sh
      - install-spoxi.sh
  - docker-run.py
    - runs given/latest instance for local investigation
    

# Changelog

## 0.0.3

- manage rules & rulesets (on/off rules, on/off rulesets)
- export
- export.log
- ruleset-history
- reload local nginx 



## 0.0.2

- basic stubs
- separate dbs
- simple init-mechanism (git clone doxi-repo)


## Roadmap / Todo

- activate dh-params in install-spoxi.sh again
- in install-spoxi.sh check if nginx already exists, exiting if so
- make default.certs in /etc/nginx/ssl/ 
- rollback on error
- email if reload or error
- doxi_admin settings
- history for rulesets
- history for rules
- global log on all changing actions
- log interface if restart is pending 
- dx_update_rules()
- test_rules via interface
- lock everything when problems with reload




