#!/bin/bash
#
#
vdir="venv" 


echo "

SPOXI - config & bin-setup 

"

echo "installing nginx-bin"

sudo cp ./nginx-config/nginx-latest /usr/sbin/nginx
sudo chmod 755 /usr/sbin/nginx 

echo "installing nginx-conf"
rsync -avzh ./nginx-config/* /etc/nginx

# TODO: react, DEACT due to debuggung
openssl dhparam -outform PEM -out  /etc/nginx/dhparams.pem 2048
sudo chown -R spoxi /etc/nginx

echo "nginx-configtest"

sudo /usr/sbin/nginx  -t -c /etc/nginx/nginx.conf

