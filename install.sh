#!/bin/bash
#
#
vdir="venv" 


echo "

SPIKE - APP_INSTALL

"
echo "[i] Initializing " 

if [ -d "$vdir" ]; then
echo "  >  old venv found, creating new" 
    rm -Rf $vdir
fi
echo "  >  installing new virtual-env in $vdir" 

echo ">  installing virtualenv in $vdir" 
virtualenv $vdir

. $vdir/bin/activate

echo ">  installing flask" 
pip install -r requirements.txt


echo ">  configuring" 

cp config.cfg.example config.cfg

./spike-server.py init


echo "
> Install OK && INIT OK
>
> read docs/install.md
> read docs/usage.md
>  - edit config.cfg and adjust port/listen-ip
>
>  - start engines: ./spike-server.py run
> 
> have fun!
>
"



