#!/usr/bin/env python

import os, time, sys

from subprocess import check_output, PIPE


print """

   _____                 _ 
  / ____|               (_)
 | (___  _ __   _____  ___ 
  \___ \| '_ \ / _ \ \/ / |
  ____) | |_) | (_) >  <| |
 |_____/| .__/ \___/_/\_\_|
        | |                
        |_|                
              Docker:Run

"""

dockers_all = check_output(["docker", "images"]).split("\n")
dockers_run = check_output(["docker", "ps"]).split("\n")
spoxi_mount = "%s/spike" % "/".join(os.getcwd().split("/")[0:-1])

if len(dockers_run) < 3:
  dockers = dockers_all
  using = "sleeping"
  for l in dockers:
    ls = l.split()
    if ls[0].strip().lower() == "REPOSITORY".lower():
      continue
    c_img = ls[2].strip()
    c_id = ls[0].strip()
    break
else:
  dockers = dockers_run
  using = "running"
  for l in dockers:
    ls = l.split()
    if ls[0].strip().lower() == "container".lower():
      continue
    c_img = ls[1].strip()
    c_id = ls[0].strip()
    break

if using == "running":
  docker_call = "docker run -it   %s " % c_img
  os.system("docker commit -m 'auto-commit after run' %s spoxi_alpha" % c_id)
else:
  docker_call = "docker run -it -p 10080:80 -p 10443:443 -p 5555:5555 -v %s:/home/spoxi/spike-nbs/spike  %s " % ( spoxi_mount, c_img)

  
print """

> using : %s 
> ID    : %s

""" % (using, c_img)





os.system(docker_call)


  
