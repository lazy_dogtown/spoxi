#!/bin/bash
# Script to install from Dockerfile

rm -Rf spoxi

rsync -avzh --exclude '*.pyc' ../../spoxi .


docker build  -t spoxi:alpha -f Dockerfile.spoxi .
