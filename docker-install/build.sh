#!/bin/bash
# Script to install needed spoxi-settings


echo "

SPOXI-Build

"

echo "> os-update and package-installation"

apt-get update
apt-get -y install openssl sudo python-virtualenv liblua5.1-0 luajit libluajit-5.1-2 geoip-bin libgeoip1 geoip-database nano rsync mc build-essential gcc python-dev
apt-get -y install  git libpcre3-dev


echo "> creating user and dirs"
user_pw=`cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1`

echo "> Spoxi-PW: $user_pw"
echo
echo ">              [ enter] to continue" 
read lol

useradd -m spoxi -p `openssl passwd -crypt '$user_pw'` -s /bin/bash

echo ">    PW set"

echo "sudo_pw spoxi / $user_pw" >> /root/info.txt




echo
echo

echo "> sudo-setup"
echo


adduser spoxi sudo

echo "
# sudoers
# defining user_groups
User_Alias NGXADMINS       = spoxi

# defining commands
Cmnd_Alias RELOAD_JOBS = /etc/init.d/nginx, \\
                /usr/sbin/nginx, \\
                /usr/local/sbin/nginx, \\
                /usr/local/nginx/sbin/nginx, \\
                /usr/sbin/service, \\
                /usr/sbin/update-rc.d

NGXADMINS ALL = (root) NOPASSWD:SETENV: RELOAD_JOBS

" >> /etc/sudoers

echo "> making dirs"
echo

mkdir -v -p /var/run/nginx
mkdir -v -p  /var/run/nginx/client_body_temp  
mkdir -v -p  /var/run/nginx/proxy_temp  
mkdir -v -p  /var/run/nginx/fastcgi_temp  
mkdir -v -p  /var/run/nginx/uwsgi_temp  
mkdir -v -p  /var/run/nginx/scgi_temp  
mkdir -v -p  /var/log/nginx  
mkdir -v -p  /etc/nginx
chown -R spoxi:spoxi /var/run/nginx  
chown -R spoxi:spoxi /var/log/nginx  
chown -R spoxi:spoxi /etc/nginx  
